package com.amazonaws.examples;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class WordCount {
    public static void main(String[] args) throws Exception {

        final String USAGE = "Usage:\n"+
                "hadoop jar WordCount-1.0-SNAPSHOT.jar com.amazonaws.examples.WordCount [Input] [Output]";

        if (args.length != 2) {
            System.err.println(USAGE);
            System.exit(1);
        }

        Job job = Job.getInstance();
        job.setJarByClass(WordCount.class);
        job.setJobName("Profile On-Boarding: WordCount");

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.setMapperClass(Map.class);
        job.setReducerClass(Reduce.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        System.exit(job.waitForCompletion(true) ? 0 : 1);

    }
}
